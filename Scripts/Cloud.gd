extends Node2D

var part_amount:float = 600
var part_time:float = 7.0
var part_delay:float = 0.035

var lc_start_x:float
var rc_start_x:float

func _ready() -> void:
	lc_start_x = $LeftCloud.position.x
	rc_start_x = $RightCloud.position.x
	
	GameState.nodes["clouds"] = self
	pass
	
func part_clouds() -> void:
	$Tween.interpolate_property($LeftCloud, "position:x", null, $LeftCloud.position.x - part_amount, part_time, Tween.TRANS_SINE, Tween.EASE_OUT)
	$Tween.interpolate_property($RightCloud, "position:x", null, $RightCloud.position.x + part_amount, part_time, Tween.TRANS_SINE, Tween.EASE_OUT)
	$Tween.interpolate_property($LeftCloud, "scale", null, Vector2(0.65, 0.65), part_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.interpolate_property($RightCloud, "scale", null, Vector2(0.65, 0.65), part_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()
	pass
	
func close_clouds() -> void:
	$Tween.interpolate_property($LeftCloud, "position:x", null, lc_start_x, part_time, Tween.TRANS_SINE, Tween.EASE_OUT)
	$Tween.interpolate_property($RightCloud, "position:x", null, rc_start_x, part_time, Tween.TRANS_SINE, Tween.EASE_OUT)
	$Tween.interpolate_property($LeftCloud, "scale", null, Vector2(1.0, 1.0), part_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.interpolate_property($RightCloud, "scale", null, Vector2(1.0, 1.0), part_time, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()
	pass
