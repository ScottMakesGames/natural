extends Area2D

var target_node
var is_alive = false

var velocity = Vector2()
var accel = 7500.0

var terminal_velocity = 2500.0

func _ready():
	
	target_node = GameState.nodes.coin_icon
	
	pass
	
func _process(delta):
	if not is_alive:
		return
		
	velocity *= 0.98
	velocity += (target_node.global_position - global_position).normalized() * accel * delta
	velocity = velocity.clamped(terminal_velocity)
	global_position += velocity * delta
	
	pass
	
func launch():
	randomize()
	var burst_size = 150
	var burst_pos = Vector2(rand_range(-burst_size, burst_size), rand_range(-burst_size, burst_size) + 50)
	
	global_position = global_position + burst_pos
	rotation_degrees = rand_range(-15.0, 15.0)
	is_alive = true
	pass
