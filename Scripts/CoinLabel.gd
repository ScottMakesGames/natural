extends Label

var current_displayed_value = 0
var time_per_number = 0.03
var animated_time = 0.0

func _ready():
	#warning-ignore:return_value_discarded
	GameState.connect("coins_updated", self, "_on_coins_updated")
	pass
	
func _process(delta: float) -> void:
	animated_time += delta
	if animated_time > time_per_number:
		if current_displayed_value != GameState.coins:
			if current_displayed_value < GameState.coins:
				if difference(current_displayed_value, GameState.coins) > 90:
					current_displayed_value += 31
				elif difference(current_displayed_value, GameState.coins) > 30:
					current_displayed_value += 11
				elif difference(current_displayed_value, GameState.coins) > 15:
					current_displayed_value += 6
				else:
					current_displayed_value += 1
			else:
				current_displayed_value -= 1
		animated_time = 0.0
			
	text = "x" + str(current_displayed_value)
	pass
	
func difference(a, b):
	return abs(a - b)

func _on_coins_updated(_new_total:int) -> void:
	pass