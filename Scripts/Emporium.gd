extends Node2D

var sprite_start_pos:Vector2
var elapsed_time:float

var item_board_slots = {}
var item_board_scene = preload("res://Scenes/ShopItem.tscn")

func _ready():
	elapsed_time = 0.0
	sprite_start_pos = $EmporiumSprite.position
	create_item_slots()
	pass

func _process(delta: float) -> void:
	elapsed_time += delta
	$EmporiumSprite.position.x = sprite_start_pos.x + (sin(elapsed_time * 0.35) * 18.0)
	$EmporiumSprite.position.y = sprite_start_pos.y + (cos(elapsed_time * 0.6) * 12.0)
	pass
	
	
func create_item_slots():
	for i in range(10):
		item_board_slots[i] = {}
		var tp = TextureProgress.new()
		$ItemBoards.add_child(tp)
		item_board_slots[i]["tp"] = tp
		tp.texture_under = preload("res://Sprites/UI/circ_progress_bg.png")
		tp.texture_progress = preload("res://Sprites/UI/circ_progress_full.png")
		tp.min_value = 1
		tp.max_value = 60
		tp.value = 1
		tp.exp_edit = true
		tp.mouse_filter = tp.MOUSE_FILTER_IGNORE
		
		var ib = item_board_scene.instance()
		$ItemBoards.add_child(ib)
		item_board_slots[i]["ib"] = ib
		var ibx = 135 * i
		var iby = 0
		if i % 2 == 1:
			iby = -230
		else:
			iby = 0
			
		ib.position = Vector2(ibx, iby)
		tp.rect_position = Vector2(ibx, iby) - Vector2(135, -120)
		tp.visible = false
		pass
	
	pass
	
# TODO:
# 
# Slots for items
# Slots populated in random order
# Countdown timer to population
# 