extends RigidBody2D

var interactive = true
var currently_held = false
var mouse_offset = Vector2()

#warning-ignore:unused_class_variable
var value = 15

var frames_since_last_mouse_motion = 0
var last_motion_recorded = Vector2(0.0, 0.0)

func _ready():
	if is_connected("input_event", self, "_on_mouse_input_event"):
		print(get_path())
	connect("input_event", self, "_on_mouse_input_event")
	pass

func _integrate_forces(state):
	if not interactive:
		return
		
	if currently_held:
		
		if !Input.is_mouse_button_pressed(BUTTON_LEFT):
			currently_held = false
			GameState.nodes.game_mode.held_item = null
			GameState.nodes.mouse_point.joint.node_b = ""
		pass
	
	linear_velocity = linear_velocity.clamped(1900)
		
	frames_since_last_mouse_motion += 1
	pass


func _on_mouse_input_event(_viewport, event, _shape_idx):
	if not interactive:
		return
		
	if event is InputEventMouseButton:
		if event.pressed and GameState.handle_click():
			currently_held = true
			GameState.nodes.game_mode.held_item = self
			# Debug.print(GameState.nodes.mouse_point.joint.node_b)
			GameState.nodes.mouse_point.joint.node_b = get_path()
		
		get_tree().set_input_as_handled()
		pass
	pass
