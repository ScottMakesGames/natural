extends Node

# this script responsible for saving and loading

var coins = 0
var click_handled = false

signal coins_updated(new_total)

var nodes = {}

func _ready():
	pass
	
func handle_click() -> bool:
	if not click_handled:
		click_handled = true
		call_deferred("unhandle_click")
		return true
	else:
		return false

func unhandle_click():
	click_handled = false
	pass
	
func add_coins(amount:int) -> void:
	coins += amount
	emit_signal("coins_updated", coins)
	pass
	
func spend_coins(amount:int) -> void:
	coins -= amount
	emit_signal("coins_updated", coins)
	pass