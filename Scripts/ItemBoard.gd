extends Node2D

var start_pos:Vector2
var elapsed_time:float
var x_wobble:float
var y_wobble:float
var radius:float = 5.0

var disabled_timer:float = 0.0
var last_dir:float = 0.0

const time_between_board_clicks = 0.15
const max_damage = 3

var damage = 0
var coin_value = 100

var board_textures = [
	preload("res://Sprites/Props/item_board_0.png"),
	preload("res://Sprites/Props/item_board_1.png"),
	preload("res://Sprites/Props/item_board_2.png"),
	preload("res://Sprites/Props/item_board_3.png")
	]

func _ready():
	randomize()
	elapsed_time = randf() * randi()
	x_wobble = rand_range(0.25, 0.3)
	y_wobble = rand_range(0.35, 0.45)
	start_pos = $BalloonBody.position
	$BalloonBody.position = _calculate_new_pos()
	
	coin_value = randi() % 36
	var cost = coin_value
	var cost_co:float = 0.0
	var cost_str:String = ""
	
	if cost >= 1000:
		cost_co = cost / 1000.0
		cost_str = "%.2fk" % cost_co
	else:
		cost_str = str(cost)
		
	var text_sizes = [
		preload("res://Fonts/item_balloon_font_0.tres"),
		preload("res://Fonts/item_balloon_font_1.tres"),
		preload("res://Fonts/item_balloon_font_2.tres"),
		preload("res://Fonts/item_balloon_font_3.tres"),
		preload("res://Fonts/item_balloon_font_4.tres")
		]
	if cost_str.length() < text_sizes.size():
		$BalloonBody/HBoxContainer/Label.add_font_override("font", text_sizes[cost_str.length()])
		
	#cost_str = "  " + cost_str
	$BalloonBody/HBoxContainer/Label.text = cost_str
	#$BalloonBody/HBoxContainer/TextureRect.set_size(Vector2(64, $BalloonBody/HBoxContainer/Label.get_line_height()))
	$BalloonBody/HBoxContainer.update()
		
	#$BoardBody.add_collision_exception_with($"/root/GameMode/Terrain/WorldCeiling")
	$BoardBody.interactive = false
	pass

func _process(delta: float) -> void:
	elapsed_time += delta
	disabled_timer -= delta
	
	var new_pos = _calculate_new_pos()
	var delta_pos : Vector2 = $BalloonBody.position - new_pos
	
	
	$BalloonBody.position.x = new_pos.x
	$BalloonBody.position.y = new_pos.y
	
	#if sign(last_dir) != sign(delta_pos.x):
	#	Debug.print("force was: " + str($BoardBody.applied_force) + " and is now zeroed")
	#	$BoardBody.applied_force = Vector2.ZERO
		
	if has_node("BoardBody"):
		$BoardBody.apply_central_impulse(Vector2(sign(delta_pos.x) * 10.0 * delta, 0.0))
	
	last_dir = delta_pos.x
	pass
	
func _calculate_new_pos() -> Vector2:
	return Vector2(
		start_pos.x + (sin(elapsed_time * x_wobble) * radius),
		start_pos.y + (cos(elapsed_time * y_wobble) * radius))

func _on_BoardBody_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event is InputEventMouseButton:
		Debug.ipr("Got a board body input event")
		
	if not has_node("BoardBody"):
		Debug.ipr("Board clicked on, contains no body (returning)")
		return
		
	if disabled_timer < 0.0 and event is InputEventMouseButton and not event.pressed and event.button_index == BUTTON_LEFT:
		var dir = $BalloonBody.global_position - get_global_mouse_position()
		disabled_timer = time_between_board_clicks
		
		Debug.ipr("Got a click on itemboard " + get_name())
		
		if GameState.coins >= coin_value:
			Debug.ipr("Player could afford, damaging")
			GameState.nodes.camera.shake(0.3)
			$BoardBody.apply_impulse(dir, Vector2(-sign(dir.x) * 190.0, 0.0))
			damage += 1
			if damage <= max_damage:
				$BoardBody/Sprite.texture = board_textures[damage]
				if damage == max_damage:
					$PinJoint2D.node_b = ""
					var bb = $BoardBody
					bb.disconnect("input_event", self, "_on_BoardBody_input_event")
					var bb_global_pos = bb.global_position
					remove_child(bb)
					$"/root/GameMode/GameObjects".add_child(bb)
					bb.global_position = bb_global_pos
					bb.interactive = true
					GameState.spend_coins(coin_value)
					
			pass
		else:
			GameState.nodes.camera.shake(0.25, 3)
			$BoardBody.apply_impulse(dir, Vector2(-sign(dir.x) * 190.0, 0.0))
	pass
