extends Node2D

onready var joint = $PinJoint2D
onready var sprite = $Sprite

func _ready():
	GameState.nodes["mouse_point"] = self
	pass

func _process(delta: float) -> void:
	global_position = get_global_mouse_position()
	
	if GameState.nodes.game_mode.current_tool == "none" or joint.node_b != "":
		sprite.visible = false
	else:
		sprite.visible = true
	
	pass