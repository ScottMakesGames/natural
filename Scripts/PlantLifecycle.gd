extends Node2D

#onready var scn_seed = preload("res://plants/seed.tscn")
var state = {
		"growth": "Seed",
		"water": 0, "water_max": 30, "water_stages": 2,
		"poop": 0, "poop_max": 20, "poop_stages": 1,
		"life_stage": 0,
		"harvest_initiated": false
	}
	
var state_defaults = {
		"water_stages": 2,
		"poop_stages": 1
	}
	
var timers = {
		"water_decay"  : 0.0,
		"water_stage"  : 0.0,
		"poop_stage"   : 0.0,
		"growth_stage" : 0.0,
		"seed_sleep"   : 0.0
	}
	
var timer_resets = {
		"water_decay"  : 5.0,
		"water_stage"  : 35.0,
		"poop_stage"   : 65.0,
		"growth_stage" : 2.0,
		"seed_sleep"   : 2.0 # 6.0
	}
	

var animation_state = 0

var base_position = Vector2()

signal water_filled
signal poop_filled

var harvested_scene

func _ready():
	
	harvested_scene = preload("res://Plants/Carrot.tscn")
	
	$"SeedBody/Sprite".z_as_relative = true
	$"SeedBody/Sprite".z_index = -1
	
	reset_timers()
	pass
	
func reset_timers() -> void:
	for k in timers.keys():
		timers[k] = timer_resets[k]
	pass
	
func process_timers(delta:float) -> void:
	match state.growth:
		"Seed":
			# processed in _physics_process()
			# timers.seed_sleep -= delta
			pass
		"Growing":
			timers.water_decay  -= delta
			timers.water_stage  -= delta
			timers.poop_stage   -= delta
			timers.growth_stage -= delta
			
			if timers.water_decay <= 0.0:
				timers.water_decay = timer_resets.water_decay
				do_water_decay()
				
			#poop_stage and water_stage timers are monitored by Callout.gd

func _process(delta: float) -> void:
	process_timers(delta)
	pass
	
func _physics_process(delta):
	if state.growth == "Seed":
		process_seed(delta)
		
	if state.growth == "Burrowing":
		process_burrowing()
		
	if state.growth == "Growing":
		process_growing()
		# TODO: move to next growth stage when all timers are complete
		# States: Growing -> NextStage -> Growing -> NextStage -> Growing -> Grown
	if state.growth == "Mature":
		process_mature()
		pass
	pass

func process_mature() -> void:
	if state.harvest_initiated:
		
		var gmp = get_global_mouse_position()
		var pull_distance = gmp.distance_to(state.harvest_point)
		
		$SeedArea/Sprite.look_at(gmp)
		$SeedArea/Sprite.rotate(PI / 2)
		$SeedArea/Sprite.scale.y = 1.0 + (pull_distance / 550)
		
		if not Input.is_mouse_button_pressed(BUTTON_LEFT):
			state.harvest_initiated = false
			$SeedArea/Sprite.scale.y = 1.0
			$SeedArea/Sprite.rotation = 0.0
			return
		#Debug.print("Harvest distance: " + str(state.harvest_point.distance_to(get_global_mouse_position())))
		# check how far between click point and wide area outer edge we are
		# map scale.y to above ratio
		# rotate sprite to match pull direction
		pass
	pass

func process_growing() -> void:
	
	if all_stages_complete():
		Debug.print("Plant is fully grown!")
		transition_to("LevellingUp")
		pass
	#else:
	#	Debug.print("Currently " + str(state.poop_stages) + " poop stages left")
	#	Debug.print("Currently " + str(state.water_stages) + " water stages left")
	#	pass
	pass

func process_seed(delta:float) -> void:
	
	$Callout.position = $SeedBody.position + Vector2(-75, -90)
	$Callout.initial_y = $SeedBody.position.y - 90
	
	var wide_field_obscured = false
	for c in $SeedBody/WideField.get_overlapping_bodies() + $SeedBody/WideField.get_overlapping_areas():
		if c.is_in_group("DontGrowNear") and not c == $SeedBody:
			wide_field_obscured = true
		pass
	
	if wide_field_obscured:
		#TODO: Yell at the user that the seed is too close to this one
		$SeedBody/Sprite.modulate = Color("#fd4e98")
		$WideAreaMarker.position = $SeedBody.position
		$WideAreaMarker.visible = true
		$Callout.activate_callout_type("no_space")
		pass
	else:
		$SeedBody/Sprite.modulate = Color("#ffffff")
		$WideAreaMarker.visible = false
		$Callout.deactivate_callout_type("no_space")
		var grounded = false
		for c in $SeedBody.get_colliding_bodies():
			if c.is_in_group("Ground"):
				grounded = true
				break
			pass
			
		if grounded:
			timers.seed_sleep -= delta
		else:
			timers.seed_sleep += delta
			
		if timers.seed_sleep < 0.0:
			transition_to("Burrowing")
	pass

func process_burrowing() -> void:
	$SeedArea.position = base_position + Vector2((randi() % 8) - 4, (randi() % 4 - 2))
	$Particles2D.position = $SeedArea.position
	pass

func transition_to(new_state):
	Debug.print("Trasnitioning from " + state.growth + " to " + new_state)
	if new_state == "Burrowing":
		var area = Area2D.new()
		area.name = "SeedArea"
		area.add_to_group("DontGrowNear")
		add_child(area)
		base_position = $SeedBody.position
		$SeedArea.transform = $SeedBody.transform
		for c in $SeedBody.get_children():
			$SeedBody.remove_child(c)
			$SeedArea.add_child(c)
			pass
		$SeedArea.collision_mask = $SeedBody.collision_mask
		$SeedArea.collision_layer = $SeedBody.collision_layer
		#warning-ignore:return_value_discarded
		$SeedArea.connect("body_entered", self, "_on_SeedArea_body_entered")
		$Particles2D.emitting = true
		$Tween.interpolate_property(self, "base_position", null, base_position - Vector2(0.0, -20.0), 1.3, Tween.TRANS_SINE, Tween.EASE_OUT)
		$Tween.interpolate_property($"SeedArea", "global_rotation_degrees", null, 0.0, 1.22, Tween.TRANS_SINE, Tween.EASE_OUT)
		$Tween.start()
		$SeedBody.queue_free()
	
	if new_state == "Growing":
		$Particles2D.emitting = false
		$Callout.position = $SeedArea.position + Vector2(-75, -90)
		$Callout.reset()
		
	if new_state == "LevellingUp":
		if animation_state == 0:
			animation_state = 1
			$Tween.stop_all()
			$Tween.remove_all()
			$Tween.interpolate_property($SeedArea/Sprite, "scale", null, Vector2(0.45, 0.45), 0.09, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
			$Tween.start()
		else:
			Debug.epr("animation_state was not zero, but we were asked to transition to levelling up from " + state.growth)
			
	if new_state == "Mature":
		# can i detect a click on both the seed area and the overlapping wide_field area?
		# if no, create the appropriate components here
		# if yes, do nothing here
		$SeedArea.connect("input_event", self, "_seed_area_input_event")
		$SeedArea/WideField.connect("input_event", self, "_wide_field_input_event")
		$SeedArea/WideField.connect("mouse_exited", self, "_wide_field_mouse_exited")
		pass
		
	state.growth = new_state
	pass
	
func _wide_field_mouse_exited():
	Debug.print("Wide area mouse exited")
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and state.harvest_initiated and GameState.handle_click():
		var pull_distance = get_global_mouse_position().distance_to(state.harvest_point)
		state.harvest_initiated = false
		Debug.ipr("HARVESTING " + str(pull_distance))
		var harvested_form = harvested_scene.instance()
		get_node("/root/GameMode/GameObjects").add_child(harvested_form)
		harvested_form.global_position = get_global_mouse_position()
		harvested_form.apply_central_impulse(Vector2(0.0, 2200.0))
		queue_free()
	else:
		state.harvest_initiated = false
		pass
	pass
	
func _wide_field_input_event(vp, ie, sh):
	if ie is InputEventMouseButton:
		Debug.print("got a click on the wide field")
	
func _seed_area_input_event(vp, ie, sh):
	if ie is InputEventMouseButton:
		if ie.pressed and state.growth == "Mature":
			Debug.ipr("Initiating plant harvest")
			state.harvest_point = get_global_mouse_position()
			state.harvest_initiated = true
			GameState.nodes.game_mode.deactivate_tool()
			GameState.handle_click()
		elif not ie.pressed:
			state.harvest_initiated = false
	pass
	
func level_up():
	state.life_stage += 1
	
	if state.life_stage == 1:
		$SeedArea/Sprite.texture = preload("res://Sprites/Plants/seed_small_sprout.png")
	if state.life_stage == 2:
		$SeedArea/Sprite.texture = preload("res://Sprites/Plants/carrot_young.png")
	if state.life_stage == 3:
		$SeedArea/Sprite.texture = preload("res://Sprites/Plants/carrot_mid.png")
	if state.life_stage == 4:
		$SeedArea/Sprite.texture = preload("res://Sprites/Plants/carrot.png")
		transition_to("Mature")
	
	reset_timers()
	reset_stages()
	return 0
	
func reset_stages():
	state.poop_stages = state_defaults.poop_stages
	state.water_stages = state_defaults.water_stages
	pass
	
func _on_SeedArea_body_entered(body) -> void:
	if body.is_in_group("tool_particle") and state.growth == "Growing":
		var t:String = body.tool_type
		var t_stage:String = t + "_stage"
		var t_stages:String = t + "_stages"
		var t_max:String = t + "_max"
		if t == "water" or t == "poop":
			if state[t] < state[t_max] and timers[t_stage] < 0.0 and state[t_stages] > 0: # currently accepting water
				state[t] += 1
				if state[t] >= state[t_max]:
					state[t] = clamp(state[t] - state[t_max], 0, state[t_max])
					
					emit_signal(t + "_filled")
					
					timers[t_stage] = timer_resets[t_stage]
					state[t_stages] -= 1
				
					if state[t_stages] == 0:
						Debug.tpr("Plant is done with " + t + "!") #TODO: Implement next stage here
						pass
						
				else: # state is less than max, so we consume particle
					$Tween.interpolate_property($"SeedArea/Sprite", "scale", Vector2(1.1, 1.35), Vector2(1.0, 1.0), 0.35, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
					$Tween.start()
					body.queue_free()
					$Callout.override_callout_priority(t)
				pass
	pass

func _on_tween_completed(_object, _key):
	if state.growth == "Burrowing":
		transition_to("Growing")
	if state.growth == "LevellingUp":
		if animation_state == 1 and _object == $SeedArea/Sprite and _key == ":scale":
			animation_state = 2
			Debug.print("Scale was: " + str($SeedArea/Sprite.scale))
			$Tween.interpolate_property($SeedArea/Sprite, "scale", Vector2(0.45, 0.45), Vector2(1.0, 1.0), 0.25, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
			$Tween.start()
			level_up()
		elif animation_state == 2 and _object == $SeedArea/Sprite and _key == ":scale":
			animation_state = 0
			transition_to("Growing")
		else:
			Debug.print("Obj: " + str(_object) + " key: " + str(_key))
	pass

func do_water_decay() -> void:
	state.water -= 1
	if state.water < 0:
		state.water = 0
	pass
	
func all_stages_complete() -> bool:
	if state.poop_stages == 0 and state.water_stages == 0:
		return true
	else:
		return false