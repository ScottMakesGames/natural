extends Area2D

var moving_left = false
var move_speed = 11.0
var wander_time = 3600.0 # seconds
var sleep_timer = wander_time

# Behaviours:
#	WANDERING
#	SLEEPING
#		GOING_TO_SLEEP
#		WAKING_UP
#	HEADING_TO_TARGET
#	COLLECTING
#	DROPPING

var current_behaviour: String = "WANDERING"

func _ready():
	set_state("WANDERING")
	pass
	
func _process(delta):	
	# implement all behaviours
	call("_perform_" + current_behaviour.to_lower(), delta)
	
	pass

func _perform_wandering(delta):
	var target = null
	
	# Effectively random target
	var water_nodes = get_tree().get_nodes_in_group("needs_water")
	var poop_nodes = get_tree().get_nodes_in_group("needs_poop")
	if water_nodes.size() > 0:
		target = water_nodes.front()
	else:
		if poop_nodes.size() > 0:
			target = poop_nodes.front()
			$Model/shell/FunnelModel.tool_type = "poop"
	
	if target != null:
		var closest = target
		for n in water_nodes:
			if $Targeter.global_position.distance_squared_to(n.global_position) < $Targeter.global_position.distance_squared_to(closest.global_position):
				closest = n
				$Model/shell/FunnelModel.tool_type = "water"
			
		for n in poop_nodes:
			if $Targeter.global_position.distance_squared_to(n.global_position) < $Targeter.global_position.distance_squared_to(closest.global_position):
				closest = n
				$Model/shell/FunnelModel.tool_type = "poop"
				
		Debug.ipr("Distance to nearest: " + str($Targeter.global_position.distance_to(closest.global_position)))
	# Target closest
	#if water_nodes.size() > 0:
	#	var closest = water_nodes.front()
		
	#	for wn in water_nodes:
	#		if position.distance_squared_to(wn.position) < position.distance_squared_to(closest.position):
	#			closest = wn
		
	#	$Model/shell/FunnelModel.tool_type = "water"
	#	target = closest
	
	#var poop_nodes = get_tree().get_nodes_in_group("needs_poop")
	#if poop_nodes.size() > 0:
	#	var closest
	#	if target == null:
	#		closest = poop_nodes.front()
	#		$Model/shell/FunnelModel.tool_type = "poop"
	#	else:
	#		closest = target
		
	#	for pn in poop_nodes:
	#		if position.distance_squared_to(pn.position) < position.distance_squared_to(closest.position):
	#			closest = pn
	#			$Model/shell/FunnelModel.tool_type = "poop"
				
	#	target = closest
			
	var dist = -1
	if target != null:
		if target.global_position.x < $Targeter.global_position.x:
			set_direction(true)
		else:
			set_direction(false)
		
		dist = $Targeter.global_position.distance_to(target.global_position)
			
	
	if dist > 50.0 or target == null:
		var x_dir = -1.0 if moving_left else 1.0
		translate(Vector2(move_speed * x_dir * delta, 0.0))
		$Model/AnimationPlayer.play("slow_walk")
	else:
		$Model/AnimationPlayer.play("idle")
	
	sleep_timer -= delta
	if sleep_timer < 0.0:
		set_state("GOING_TO_SLEEP")
	pass
	
func _perform_sleeping(delta:float):
	# see _on_Shelldon_input_event()
	pass
	
func _perform_going_to_sleep(delta:float):
	if not $Model/AnimationPlayer.is_playing():
		set_state("SLEEPING")
	pass
	
func _perform_waking_up(delta:float):
	if not $Model/AnimationPlayer.is_playing():
		set_state("WANDERING")
	pass
	
func _perform_idle(delta:float):
	pass

func _on_Shelldon_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and not event.pressed:
		if current_behaviour == "SLEEPING":
			sleep_timer = wander_time
			set_direction(randi() % 2 == 1)
			set_state("WAKING_UP")
	pass

func _on_Shelldon_body_entered(body):
	if body.is_in_group("OtoBoundary"):
		set_direction(!moving_left)
	pass
	
func set_direction(is_left):
	if moving_left != is_left:
		$Tween.stop_all()
		$Tween.interpolate_property($Model, "scale:x", null, -1.0 if is_left else 1.0, 0.4, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
		$Tween.start()
		moving_left = is_left
	pass
	
func set_state(new_state:String):
	var anim = ""
	match new_state:
		"WANDERING":
			anim = "slow_walk"
		"GOING_TO_SLEEP":
			anim = "going_to_sleep"
		"SLEEPING":
			anim = "sleeping"
		"WAKING_UP":
			anim = "waking_up"
		"IDLE":
			anim = "idle"
	
	if anim != "":
		$Model/AnimationPlayer.play(anim)
		
	current_behaviour = new_state