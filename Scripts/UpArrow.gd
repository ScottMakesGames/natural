extends Sprite

export var is_down:bool = false

var start_y : float
var elapsed_time : float
var bob_speed : float
var bob_height : float

func _ready():
	start_y = position.y
	elapsed_time = 0.0
	bob_speed = 5.5
	bob_height = 45.0
	
	if is_down:
		scale.y = -1
		
	$Tween.connect("tween_completed", self, "_tween_completed")
	pass
	
func _tween_completed(obj, key) -> void:
	self.visible = false
	pass
	
func _process(delta: float) -> void:
	elapsed_time += delta
	position.y = start_y + (sin(elapsed_time * bob_speed) * bob_height)
	
	if not self.visible:
		#Debug.print(GameState.nodes.camera.tween_state + " " + str(self.visible))
		if GameState.nodes.camera.tween_state == "going_up" and is_down:
				self.visible = true
		if GameState.nodes.camera.tween_state == "going_down" and not is_down:
				self.visible = true
				
			
	pass


func _on_Area2D_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if not self.visible:
		return
	if event is InputEventMouseButton and event.is_pressed():
		if GameState.handle_click():
			var mod:float = -1.0 if is_down else 1.0
			$Tween.interpolate_property(self, "scale", Vector2(1.22, 1.22 * mod), Vector2(1.0, 1.0 * mod), 0.65, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
			if is_down:
				GameState.nodes.camera.transition_camera_down()
			else:
				GameState.nodes.camera.transition_camera_up()
			$Tween.start()
			
	pass
